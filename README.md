# optAd360 recruitment

Wykorzystując framework Symfony 4/5 zaprojektuj aplikację pobierającą dane z wielu systemów zewnętrznych, komunikujących się poprzez API.

Na potrzeby niniejszego zadania jako jeden z zewnętrznych systemów przygotowane zostało testowe API które jest dostępne pod [następującym linkiem](https://api.optad360.com/testapi).

Aplikacja ma być odpowiedzialna za pobranie oraz zapis do bazy danych.
Częścią zadania jest również zaprojektowanie struktury bazy danych.

W realizacji zadania należy wykorzystać następujące technologie:
* Docker,
* PHPUnit,
* MySQL/PostgreSQL,
* GIT,
* Composer

## Installation

* Clone repository to your `projects` directory.
* Edit `<project_name>/php/app/.env` file and fill those values.
```
DATABASE_URL=
```
* Go to `<project_name>/docker/logs/nginx` and remove `.dist` extension from both files.
* Now, you can run the app. Open terminal, go to the project directory and use below commands:
```
docker-compose up -d --build
```
```
docker exec -it <fpm_container_name> sh
> composer install
> exit
```
OK. That's it! Open your browser on `localhost` page and start fetching data.
