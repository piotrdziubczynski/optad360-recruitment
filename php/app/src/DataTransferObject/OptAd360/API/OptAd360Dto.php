<?php

declare(strict_types=1);

namespace App\DataTransferObject\OptAd360\API;

use App\Entity\Advert;
use DateTimeInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class OptAd360Dto
 *
 * @package App\DataTransferObject\OptAd360\API
 */
final class OptAd360Dto
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public ?string $url = '';

    /**
     * @Assert\Type("string")
     * @Assert\NotNull()
     */
    public ?string $tag = null;

    /**
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    public ?DateTimeInterface $dateStart = null;

    /**
     * @Assert\Type("float")
     * @Assert\PositiveOrZero()
     */
    public ?float $estimatedRevenue = 0.0;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public ?string $currency = 'PLN';

    /**
     * @Assert\Type("int")
     * @Assert\PositiveOrZero()
     */
    public ?Advert $advert = null;

    /**
     * @Assert\Type("int")
     * @Assert\PositiveOrZero()
     */
    public ?int $impressions = 0;

    /**
     * @Assert\Type("float")
     * @Assert\PositiveOrZero()
     */
    public ?float $eCPM = 0.0;

    /**
     * @Assert\Type("int")
     * @Assert\PositiveOrZero()
     */
    public ?int $clicks = 0;

    /**
     * @Assert\Type("float")
     * @Assert\PositiveOrZero()
     */
    public ?float $CTR = 0.0;
}
