<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200830154334 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE action (id INT AUTO_INCREMENT NOT NULL, advert_id INT NOT NULL, impressions INT DEFAULT NULL, e_cpm DOUBLE PRECISION DEFAULT NULL, clicks INT DEFAULT NULL, ctr DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_47CC8C92D07ECCB6 (advert_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE advert (id INT AUTO_INCREMENT NOT NULL, url VARCHAR(255) NOT NULL, tag VARCHAR(255) DEFAULT NULL, date_start DATETIME NOT NULL, estimated_revenue DOUBLE PRECISION NOT NULL, currency ENUM(\'EUR\', \'PLN\'), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C92D07ECCB6 FOREIGN KEY (advert_id) REFERENCES advert (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE action DROP FOREIGN KEY FK_47CC8C92D07ECCB6');
        $this->addSql('DROP TABLE action');
        $this->addSql('DROP TABLE advert');
    }
}
