<?php

namespace App\Entity;

use App\DataTransferObject\OptAd360\API\OptAd360Dto;
use App\Repository\ActionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActionRepository::class)
 */
class Action
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Advert::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="advert_id", referencedColumnName="id", nullable=false)
     */
    private $advert;

    /**
     * @ORM\Column(type="integer")
     */
    private $impressions;

    /**
     * @ORM\Column(type="float")
     */
    private $eCPM;

    /**
     * @ORM\Column(type="integer")
     */
    private $clicks;

    /**
     * @ORM\Column(type="float")
     */
    private $CTR;

    /**
     * Action constructor.
     */
    public function __construct()
    {
        $this->id = 0;
        $this->advert = null;
        $this->impressions = 0;
        $this->eCPM = 0.0;
        $this->clicks = 0;
        $this->CTR = 0.0;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getAdvert(): ?int
    {
        return $this->advert;
    }

    /**
     * @return int|null
     */
    public function getImpressions(): ?int
    {
        return $this->impressions;
    }

    public function addImpression(): void
    {
        $this->impressions++;
    }

    /**
     * @return float|null
     */
    public function getECPM(): ?float
    {
        return $this->eCPM;
    }

    /**
     * @return int|null
     */
    public function getClicks(): ?int
    {
        return $this->clicks;
    }

    public function addClick(): void
    {
        $this->clicks++;
    }

    /**
     * @return float|null
     */
    public function getCTR(): ?float
    {
        return $this->CTR;
    }

    /**
     * @param OptAd360Dto $dto
     */
    public function createFromAPI(OptAd360Dto $dto)
    {
        $this->advert = $dto->advert;
        $this->impressions = $dto->impressions;
        $this->eCPM = $dto->eCPM;
        $this->clicks = $dto->clicks;
        $this->CTR = $dto->CTR;
    }
}
