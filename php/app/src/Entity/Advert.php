<?php

namespace App\Entity;

use App\DataTransferObject\OptAd360\API\OptAd360Dto;
use App\Repository\AdvertRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass=AdvertRepository::class)
 */
class Advert
{
    private static $passCurrencies = [
        'EUR',
        'PLN',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $tag;

    /**en
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $dateStart;

    /**
     * @ORM\Column(type="float")
     */
    private ?float $estimatedRevenue;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $currency;

    /**
     * Advert constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->id = 0;
        $this->url = '';
        $this->tag = null;
        $this->dateStart = new DateTimeImmutable();
        $this->estimatedRevenue = 0.0;
        $this->currency = 'PLN';
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateStart(): ?DateTimeInterface
    {
        return $this->dateStart;
    }

    /**
     * @return float|null
     */
    public function getEstimatedRevenue(): ?float
    {
        return $this->estimatedRevenue;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param OptAd360Dto $dto
     */
    public function createFromAPI(OptAd360Dto $dto)
    {
        $this->url = $dto->url;
        $this->tag = $dto->tag;
        $this->dateStart = $dto->dateStart;
        $this->estimatedRevenue = $dto->estimatedRevenue;
        $this->currency = $dto->currency;
    }
}
