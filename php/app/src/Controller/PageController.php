<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Manager\OptAd360\OptAd360Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController
 *
 * @package App\Controller
 */
final class PageController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"})
     * @Route("/homepage", name="app:page:home", methods={"GET"})
     *
     * @param Request $request
     * @param OptAd360Manager $manager
     *
     * @return Response
     */
    public function home(Request $request, OptAd360Manager $manager): Response
    {
        return $this->render('page/homepage.html.twig', [
            'adverts' => $manager->getAdverts(),
        ]);
    }
}
