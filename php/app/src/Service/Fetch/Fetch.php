<?php

declare(strict_types=1);

namespace App\Service\Fetch;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Throwable;

/**
 * Class Fetch
 *
 * @package App\Service\Fetch
 */
final class Fetch implements FetchInterface
{
    /**
     * @var Client $client
     */
    private Client $client;

    /**
     * @var ResponseInterface|null $response
     */
    private ?ResponseInterface $response;

    /**
     * Fetch constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->response = null;
    }

    /**
     * @inheritDoc
     */
    public function get(string $uri, array $options = []): void
    {
        $this->request('GET', $uri, $options);
    }

    /**
     * @inheritDoc
     */
    public function post(string $uri, array $options = []): void
    {
        $this->request('POST', $uri, $options);
    }

    /**
     * @inheritDoc
     */
    public function getBody(): ?StreamInterface
    {
        if ($this->response === null) {
            return null;
        }

        return $this->response->getBody();
    }

    /**
     * @inheritDoc
     */
    public function getContents(): ?string
    {
        $body = $this->getBody();

        if ($body === null) {
            return null;
        }

        return $body->getContents();
    }

    /**
     * @inheritDoc
     */
    public function getStatus(): ?int
    {
        if ($this->response === null) {
            return null;
        }

        return $this->response->getStatusCode();
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     */
    private function request(string $method, string $uri, array $options): void
    {
        $response = null;

        try {
            $response = $this->client->request($method, $uri, $options);
        } catch (Throwable $e) {
            // todo: add Log
        }

        $this->response = $response;
    }
}
