<?php

declare(strict_types=1);

namespace App\Service\Fetch;

use Psr\Http\Message\StreamInterface;

interface FetchInterface
{
    /**
     * @param string $uri
     * @param array $options
     */
    public function get(string $uri, array $options = []): void;

    /**
     * @param string $uri
     * @param array $options
     */
    public function post(string $uri, array $options = []): void;

    /**
     * @return StreamInterface|null
     */
    public function getBody(): ?StreamInterface;

    /**
     * @return string|null
     */
    public function getContents(): ?string;

    /**
     * @return int
     */
    public function getStatus(): ?int;
}
