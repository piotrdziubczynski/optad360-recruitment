<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200830220155 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE action CHANGE impressions impressions INT NOT NULL, CHANGE e_cpm e_cpm DOUBLE PRECISION NOT NULL, CHANGE clicks clicks INT NOT NULL, CHANGE ctr ctr DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE advert DROP date_start, CHANGE currency currency VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE action CHANGE impressions impressions INT DEFAULT NULL, CHANGE e_cpm e_cpm DOUBLE PRECISION DEFAULT NULL, CHANGE clicks clicks INT DEFAULT NULL, CHANGE ctr ctr DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE advert ADD date_start DATETIME NOT NULL, CHANGE currency currency VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
