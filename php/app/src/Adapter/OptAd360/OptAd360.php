<?php

declare(strict_types=1);

namespace App\Adapter\OptAd360;

use App\Service\Fetch\Fetch;
use App\Service\Fetch\FetchInterface;
use DateTimeInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\StreamInterface;

/**
 * Class OptAd360
 *
 * @package App\Adapter\OptAd360
 */
final class OptAd360
{
    private FetchInterface $fetch;
//    private static string $key = 'HJGHcZvJHZhjgew6qe67q6GHcZv3fdsAqxbvB33fdV';
//    private static string $uri = 'https://api.optad360.com/get';
    private static string $uri = 'https://api.optad360.com/testapi';

    /**
     * OptAd360 constructor.
     */
    public function __construct()
    {
        $this->fetch = new Fetch();
    }

    /**
     * @param DateTimeInterface|null $startDate
     * @param DateTimeInterface|null $endDate
     */
    public function fetchData(?DateTimeInterface $startDate = null, ?DateTimeInterface $endDate = null): void
    {
//        $query = [
//            'key' => static::$key,
//            'output' => 'json',
//        ];
//
//        if ($startDate !== null) {
//            $query['startDate'] = $startDate->format('Y-m-d');
//        }
//
//        if ($endDate !== null) {
//            $query['endDate'] = $endDate->format('Y-m-d');
//        }
        $query = [];

        $this->fetch->get(static::$uri, [
            RequestOptions::QUERY => $query,
        ]);
    }

    /**
     * @return StreamInterface|null
     */
    public function getBody(): ?StreamInterface
    {
        return $this->fetch->getBody();
    }

    /**
     * @return string|null
     */
    public function getContents(): ?string
    {
        return $this->fetch->getContents();
    }
}
