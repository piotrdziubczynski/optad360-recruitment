<?php

declare(strict_types=1);

namespace App\Service\Manager\OptAd360;

use App\Adapter\OptAd360\OptAd360;
use App\DataTransferObject\OptAd360\API\OptAd360Dto;
use App\Entity\Action;
use App\Entity\Advert;
use App\Repository\AdvertRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use Exception;

/**
 * Class OptAd360Manager
 *
 * @package App\Service\Manager\OptAd360
 */
final class OptAd360Manager
{
    private OptAd360 $adapter;

    private ObjectRepository $repository;

    private ObjectManager $manager;

    /**
     * OptAd360Manager constructor.
     *
     * @param OptAd360 $adapter
     * @param AdvertRepository $repository
     * @param EntityManagerInterface $manager
     */
    public function __construct(OptAd360 $adapter, AdvertRepository $repository, EntityManagerInterface $manager)
    {
        $this->adapter = $adapter;
        $this->repository = $repository;
        $this->manager = $manager;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAdverts(): array
    {
//        $adverts = $this->repository->getAdverts();
        $adverts = [];

        if (empty($adverts)) {
            $this->adapter->fetchData();
            $adverts = $this->dataMapper($this->adapter->getContents());
            $adverts = $this->addAdverts($adverts);
        }

        return $adverts;
    }

    /**
     * @param array $adverts
     *
     * @return array
     * @throws Exception
     */
    public function addAdverts(array $adverts): array
    {
        $i = 0;

        /** @var OptAd360Dto $dto */
        foreach ($adverts as $dto) {
            $advert = new Advert();
            $advert->createFromAPI($dto);

            $this->manager->persist($advert);
            $dto->advert = $advert;

            $action = new Action();
            $action->createFromAPI($dto);

            $this->manager->persist($action);

            if (++$i >= 25) {
                $this->manager->flush();
                $i = 0;
            }
        }

        if ($i > 0) {
            $this->manager->flush();
        }

        return $adverts;
    }

    /**
     * @param string $json
     *
     * @return array
     * @throws Exception
     */
    private function dataMapper(string $json): array
    {
        $object = json_decode($json, true);
        $settings = $object['settings'] ?? [];
        $header = $object['headers'] ?? [];
        $data = $object['data'] ?? [];

        $adverts = [];

        /**
         * todo: change below code to validate via Form
         */
        foreach ($data as $key => $values) {
            $url = $values[0] ?? '';
            $tag = $values[1] ?? '';
            $dateStart = DateTimeImmutable::createFromFormat('Y-m-d', $values[2] ?? '');
            $estimatedRevenue = $values[3] ?? 0.0;
            $currency = $settings['currency'] ?? '';
            $impressions = $values[4] ?? 0;
            $eCPM = $values[5] ?? 0.0;
            $clicks = $values[6] ?? 0;
            $CTR = $values[7] ?? 0.0;

            $advert = new OptAd360Dto();
            $advert->url = $url;
            $advert->tag = $tag;
            $advert->dateStart = $dateStart ?: new DateTimeImmutable();
            $advert->estimatedRevenue = $estimatedRevenue;
            $advert->currency = $currency;
            $advert->impressions = $impressions;
            $advert->eCPM = $eCPM;
            $advert->clicks = $clicks;
            $advert->CTR = $CTR;

            $adverts[] = $advert;
        }

        return $adverts;
    }
}
